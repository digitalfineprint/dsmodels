# Data Science Models #

This repository is the location for all of the data science models currently in use at Digital Fineprint. These include:

 - Digital Saviness
 - Industry Prediction
 - Revenue Estimation
 - Dissolution Score

Models yet to be built:
 
 - Employee estimation

### Structure ###

Under each respective folder the projects are structured as followed.
 
 - Root Folder
 	- README
		- Description of problem
		- Description of data used
		- Performance metrics
		- Improvements to be made
	- Model artifacts
		- .pickle files
	- Training
		- TrainingScript.py

Each respective folder should contain all information required to:
 
 - Understand the project
 - Visit the data