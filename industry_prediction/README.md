# Indutstry Prediction

# About
Indutstry Prediction predicts the industry type of a business with regards to the following multi-class classification categories:

- wholesale_and_retail
- general_contractor
- professional_services
- charity_club
- information_technology
- construction

The model accepts a JSON string as an input, for example:

` payload = json.dumps({'instances':['Welcome to davids Italian resturant, here we serve a variety of Northern Italian Cuisine']}) `

Where instances is a string value a dictionary key, and the value field is a list of strings (text).
