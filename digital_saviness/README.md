# Digital Saviness

## About ##

This model uses the google lighthouse data found in the datalake. It comproses the following data points:

    - Google SEO Score
    - Google Best Practices Sore
    - Google Performance Score
    - Google Accessibility Score
    - Google PWA Score

These data points are collected through: https://web.dev/measure/

## Methodology ##

The model is an athena query that uses different weights to calculate an aggregated score. the score has a maximum possible value of 100 indicating that the website has max values in all categories.


